//let's connect controllers here
//but need to set up first
//Middleware since endpoints parin pinaguusapan



const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController")
//router allows us to get access to http method
//allows us to create a router instance that functions as a middleware and routing system
//allows access to HTTP method middleswares that makes it easier to create routes for our application

//Route to get all the tasks
router.get("/", (req, res) => {
	//get all task function
	//call the task controller
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})


//Create a route for creating a task

router.post("/", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

//Create a route for deleting task
//delete thru ObjectId
//:id is parameter lang called wildcard
//http://localhost:3001/tasks/:id
//The colon is an identifier that helps create a dynamic route which allows us to supply information in the URL
//The world that comes after the colon symbol will be the name of the URL parameter
//:id is called WILDCARD where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
router.delete("/:UserId", (req, res) => {
	//URL parameter values are accessed via the request oject's "params" property
	//The property name of this object will match the given URL parameter name
	//In this case "id" is the 
	taskController.deleteTask(req.params.UserId).then(result => res.send(result));
})


//Update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})


//Activity starts here
//get specific task number1

router.get("/:id", (req, res) => {
	
	taskController.getSpecificTask(req.params.id, req.body.status).then(result => res.send(result));
})


//change status
router.put("/:id/completed", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result))
})




module.exports = router;


//Si routes ay coconect kay index.js
