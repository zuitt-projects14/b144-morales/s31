//Dito function and business logic
//Meaning lahat ng controller andito

//Get Task module
//Controllers contain the function and business logic of our express js app
//Meaning all the operations can do will be placed in this file
const Task = require("../models/task")


//Controller function for getting all the tasks
//galing kay task routes
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

//Controller function for creating a task
module.exports.createTask = (requestBody) =>{
	//create a task object based on the Mongoose model "Task"
	let newTask = new Task({
		name: requestBody.name
	})
	//Save
	//The "then" method will accept 2 parameters
	//First parameter if ok na. the first parameter will store the result returned by the mongooese "save" method
	//second parameter the error. The second parameter will store the error object.
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error)
			//if an error is encountered, the return statement will prevent any other line or code below and within the same code block from executing.
			//Since the following return statement is nested within the "then" method chaned to the "save" method, they do not prevent each other from executing code
			//the else statement will no longer be evaluated kapag di tayo naglagay ng return
			return false;
		}else {
			//if save is successful return the new task object
			return task;
		}
	})
}

//Controller function for deleting a task

/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/Route
2. Delete the task

*/
//pikachu ang taskId
module.exports.deleteTask = (taskId) => {
	//The findByIdandRemove mongoose method that will look for a task with the same id provided from the URL and remove/delete the document from the MongoDB. It looks for the document using the "_id" field
return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{
	if(err){
	console.log(err);
	return false;
}else{
	return removedTask;
}

}
)
}


//updating a tasks data

/*
Business Logic
1. Get the task with the Id
2. Replace the task's name returned from the database with the "name" property from the request body
3. save the task
*/

module.exports.updateTask =(taskId, newContent) => {
		return Task.findById(taskId).then((result, error) => {
			//if an error is encountered, return a false
			if(error){
			console.log(error);
			return false
		}
		//Results of the findById method will be stored in the result parameter
		//It's name property will be reassigned the value of the "name" received from the request
		result.status = newContent.status;
		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return false
			}else{
				return updatedTask;
			}
		})
		})
	}

//Activity Number 1

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then((getSpecificTask, err) =>{
	if(err){
	console.log(err);
	return false;
}else{
	return getSpecificTask;
}

})
}



//change status
module.exports.updateStatus =(taskId, newContent) => {
		return Task.findById(taskId).then((result, error) => {
	
			if(error){
			console.log(error);
			return false
		}
		
		result.status = newContent.status;
		return result.save().then((updatedStatus, error) => {
			if(error){
				console.log(error)
				return false
			}else{
				return updatedStatus;
			}
		})
		})
	}

















